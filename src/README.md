# How to install CMS Bundle
1. Add bundle in **bundles.php**
2. Add in **routes.yaml**:
```yaml
cms:
    resource: '@CmsBundle/Resource/config/routing.yaml'
```
3. Add in **api_platform.yaml**:
```yaml
api_platform:
  mapping:
    paths: ['%kernel.project_dir%/vendor/dexodus/cms-bundle/src/Entity']
```
4. Add in **doctrine.yaml**:
```yaml
doctrine:
  orm:
    mappings:
      CmsBundle:
        is_bundle: true
        type: attribute
        dir: 'Entity'
        prefix: 'Dexodus\CmsBundle\Entity'
        alias: Dexodus\CmsBundle
```
4. Add in **entity_form.yaml**:
```yaml
entity_form:
    mapping:
        -   dir: '%kernel.project_dir%/vendor/dexodus/cms-bundle/src/Entity'
            prefix: 'Dexodus\CmsBundle\Entity'
```
