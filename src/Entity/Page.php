<?php

declare(strict_types=1);

namespace Dexodus\CmsBundle\Entity;

use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use ApiPlatform\Metadata\Post;
use ApiPlatform\Metadata\Put;
use Dexodus\CmsBundle\Repository\PageRepository;
use Dexodus\CmsBundle\State\PageProcessor;
use Dexodus\EntityFormBundle\Attribute\EntityForm;
use Dexodus\EntityFormBundle\Attribute\EntityFormField;
use Dexodus\EntityFormBundle\Attribute\Title;
use Dexodus\EntityFormBundle\Dto\EntityFormMode;
use Dexodus\EntityFormBundle\Enum\EntityFormFieldComponentEnum;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

#[ApiResource(operations: [
    new Post(denormalizationContext: ['groups' => ['cms']], processor: PageProcessor::class),
    new Put(denormalizationContext: ['groups' => ['cms']], processor: PageProcessor::class),
    new Get(normalizationContext: ['groups' => ['cms']]),
    new GetCollection(normalizationContext: ['groups' => ['cms']]),
])]
#[ORM\Entity(repositoryClass: PageRepository::class)]
#[EntityForm(modes: [
    new EntityFormMode('create', ['cms']),
    new EntityFormMode('edit', ['cms']),
])]
class Page
{
    #[ORM\Id, ORM\Column, ORM\GeneratedValue]
    public readonly int $id;

    #[Groups(['cms'])]
    #[ORM\Column]
    #[Assert\NotBlank(message: 'validation.not_blank')]
    #[Title('Путь страницы')]
    public string $path;

    #[Groups(['cms'])]
    #[ORM\Column(type: 'text')]
    #[Assert\NotBlank(message: 'validation.not_blank')]
    #[Title('Заголовок страницы')]
    public string $title;

    #[Groups(['cms'])]
    #[ORM\Column(type: 'text')]
    #[EntityFormField(component: EntityFormFieldComponentEnum::CKEDITOR_FIELD)]
    #[Title('Контент страницы')]
    public string $content;
}
