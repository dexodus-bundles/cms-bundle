<?php

declare(strict_types=1);

namespace Dexodus\CmsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class CmsBundle extends Bundle
{
}
