<?php

declare(strict_types=1);

namespace Dexodus\CmsBundle\Service;

class PathNormalizer
{
    public function normalize(string $path): string
    {
        $path = trim($path, '/');
        $path = mb_strtolower($path);

        return '/' . $path . '/';
    }
}
