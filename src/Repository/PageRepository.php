<?php

declare(strict_types=1);

namespace Dexodus\CmsBundle\Repository;

use Dexodus\CmsBundle\Entity\Page;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Page|null find($id, $lockMode = null, $lockVersion = null)
 * @method Page|null findOneBy(array $criteria, ?array $orderBy = null)
 * @method Page[] findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
 * @method Page[] findAll()
 */
class PageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    public function findByPath(string $path): Page|null
    {
        return $this->findOneBy(['path' => $path]);
    }
}
