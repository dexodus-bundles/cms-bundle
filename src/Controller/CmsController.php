<?php

declare(strict_types=1);

namespace Dexodus\CmsBundle\Controller;

use Dexodus\CmsBundle\Repository\PageRepository;
use Dexodus\CmsBundle\Service\PathNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class CmsController
{
    public function __construct(
        private SerializerInterface $serializer,
        private PageRepository      $pageRepository,
        private PathNormalizer      $pathNormalizer,
    )
    {
    }

    #[Route('/cms/get-page/{path}', name: 'dexodus.cms_bundle.cms.get_page', requirements: ['path' => '.+'])]
    public function getPage(string $path): Response
    {
        $page = $this->pageRepository->findByPath($this->pathNormalizer->normalize($path));

        if (is_null($page)) {
            throw new NotFoundHttpException();
        }

        $result = $this->serializer->serialize($page, 'json');

        return new Response($result, 200, ['Content-Type' => 'application/json']);
    }
}
