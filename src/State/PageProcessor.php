<?php

declare(strict_types=1);

namespace Dexodus\CmsBundle\State;

use ApiPlatform\Doctrine\Common\State\PersistProcessor;
use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProcessorInterface;
use Dexodus\CmsBundle\Entity\Page;
use Dexodus\CmsBundle\Service\PathNormalizer;
use Symfony\Component\DependencyInjection\Attribute\Autowire;

class PageProcessor implements ProcessorInterface
{
    private PersistProcessor $persistProcessor;
    private PathNormalizer $pathNormalizer;

    public function __construct(
        #[Autowire(service: PersistProcessor::class)] PersistProcessor $persistProcessor,
        #[Autowire(service: PathNormalizer::class)] PathNormalizer $pathNormalizer,
    )
    {
        $this->persistProcessor = $persistProcessor;
        $this->pathNormalizer = $pathNormalizer;
    }

    /** @param Page $data */
    public function process(mixed $data, Operation $operation, array $uriVariables = [], array $context = [])
    {
        $data->path = $this->pathNormalizer->normalize($data->path);

        return $this->persistProcessor->process($data, $operation, $uriVariables, $context);
    }
}
